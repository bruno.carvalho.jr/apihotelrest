package com.hotel.APIHotelRest.controllers;

import java.text.ParseException;
import java.util.ArrayList;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.hotel.APIHotelRest.models.Hotel;
import com.hotel.APIHotelRest.models.HotelDetail;

@RestController
@RequestMapping(value="/hotels")
public class Hotels {
	
	RestTemplate template = new RestTemplate();
	
	String uriPath = "";
	
	UriComponents uri = UriComponentsBuilder.newInstance()
			.scheme("https")
			.host("cvcbackendhotel.herokuapp.com/")
			.build();

	
	//Gera o valor calculado da viagem para todos os hoteis da cidade.
	//PS: duvida: os valores de codigo da cidade e datas seriam passadas pelo front como String ou int/Date? tratei como string
	@GetMapping("/byCity")
	public ArrayList<HotelDetail> getPricesByCity(String city, String checkin, String checkout, Long adult, Long child) throws ParseException{
		
		String cityCode = this.getCityCode(city);
		
		Hotel[] hoteis = this.getListHotelsByCity(cityCode);
		
		ArrayList<HotelDetail> hotelsDetail = new ArrayList<HotelDetail>(); 
				
		for(Hotel hotel : hoteis) {
			
			HotelDetail hotelDetail = hotel.calcDetailPrice(checkin, checkout, adult, child);
			
			hotelDetail.id = hotel.id;
			
			hotelDetail.cityName = hotel.cityName;
			
			hotelsDetail.add(hotelDetail);
			
		}
		
		return hotelsDetail;
	}
	
	//busca na api broker valores de hoteis na cidade
	public Hotel[] getListHotelsByCity(String codeCity){
		
		String uriPath = "hotels/avail/";
		
		ResponseEntity<Hotel[]> responseEntity = template.getForEntity(uri.toUriString() 
				+ uriPath + codeCity, Hotel[].class);
		
		Hotel[] hotels = responseEntity.getBody();
	
		return hotels;
	}
	
	//Gera o valor calculado da viagem o hotel selecionado
	//PS: duvida: os valores de codigo da cidade e datas seriam passadas pelo front como String ou int/Date? tratei como string
	@GetMapping("/details/byCode")
	public HotelDetail getPricesByCode(Long hotelId, String checkin, String checkout, Long adult, Long child) throws ParseException{
		
		Hotel[] hoteis = this.getDetailHotelByCode(hotelId);
		
		ArrayList<HotelDetail> hotelsDetail = new ArrayList<HotelDetail>(); 
				
		for(Hotel hotel : hoteis) {
			
			HotelDetail hotelDetail = hotel.calcDetailPrice(checkin, checkout, adult, child);
			
			hotelDetail.cityName = hotel.cityName;
			
			hotelDetail.id = hotel.id;
			
			hotelsDetail.add(hotelDetail);
			
		}
		
		return hotelsDetail.get(0);
	}
	
	//busca na api valores do hotel selecionado
	public Hotel[] getDetailHotelByCode(Long hotelId){
		
		String uriPath = "hotels/";
		
		ResponseEntity<Hotel[]> responseEntity = template.getForEntity(uri.toUriString()
				+ uriPath + hotelId.toString(), Hotel[].class);
		
		Hotel[] hotels = responseEntity.getBody();
	
		return hotels;
	}
	
	//Usa a string da cidade passada para retornar o codigo da cidade (PS: eu trataria isto no frontend trazendo diretamente o codigo para o backend)
	public String getCityCode(String city) {
		
		String codeCity = "";
		
		switch(city.toLowerCase()) {
		  case "são paulo":
		    
			  codeCity = "9626";
		  case "rio de janeiro":
		    
			  codeCity = "7110";
		  case "porto seguro":
		    
			  codeCity = "1032";
		}
		
		return codeCity;
	}

}
