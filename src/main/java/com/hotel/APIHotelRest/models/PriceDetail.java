package com.hotel.APIHotelRest.models;

import java.math.BigDecimal;

public class PriceDetail {
	
	public BigDecimal pricePerDayAdult;
	
	public BigDecimal pricePerDayChild;
	

	public BigDecimal getPricePerDayAdult() {
		return pricePerDayAdult;
	}

	public void setPricePerDayAdult(BigDecimal pricePerDayAdult) {
		this.pricePerDayAdult = pricePerDayAdult;
	}

	public BigDecimal getPricePerDayChild() {
		return pricePerDayChild;
	}

	public void setPricePerDayChild(BigDecimal pricePerDayChild) {
		this.pricePerDayChild = pricePerDayChild;
	}
	
	

}
