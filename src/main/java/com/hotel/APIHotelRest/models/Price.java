package com.hotel.APIHotelRest.models;

import java.io.Serializable;
import java.math.BigDecimal;

public class Price {
	
	public BigDecimal adult;
	
	public BigDecimal child;
	
	

	public BigDecimal getAdult() {
		return adult;
	}

	public void setAdult(BigDecimal adult) {
		this.adult = adult;
	}

	public BigDecimal getChild() {
		return child;
	}

	public void setChild(BigDecimal child) {
		this.child = child;
	}

}
