package com.hotel.APIHotelRest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiHotelRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiHotelRestApplication.class, args);
	}

}
