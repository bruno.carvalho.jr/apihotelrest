package com.hotel.APIHotelRest.models;

import java.io.Serializable;
import java.math.BigDecimal;

public class RoomDetail {
	
	public long roomID;
	
	public String categoryName;
	
	public BigDecimal totalPrice;
	
	public PriceDetail priceDetail;
	
	public RoomDetail() {
		
		this.priceDetail = new PriceDetail();
	}
	
	

	public long getRoomID() {
		return roomID;
	}

	public void setRoomID(long roomID) {
		this.roomID = roomID;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public PriceDetail getPriceDetail() {
		return priceDetail;
	}

	public void setPriceDetail(PriceDetail priceDetail) {
		this.priceDetail = priceDetail;
	}
	

}
