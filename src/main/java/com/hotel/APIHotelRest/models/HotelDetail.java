package com.hotel.APIHotelRest.models;

import java.util.ArrayList;

public class HotelDetail {

	public long id;
	
	public String cityName;
	
	public ArrayList<RoomDetail> rooms;
	
	public HotelDetail() {
		
		ArrayList<RoomDetail> rooms = new ArrayList<RoomDetail>();
		this.rooms = rooms;
	}
	
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public ArrayList<RoomDetail> getRooms() {
		return rooms;
	}

	public void setRooms(ArrayList<RoomDetail> rooms) {
		this.rooms = rooms;
	}
	
}