package com.hotel.APIHotelRest.models;

import java.io.Serializable;

public class Room {
	
	public long roomID;
	
	public String categoryName;
	
	public Price price;
	

	public long getRoomID() {
		return roomID;
	}

	public void setRoomID(long roomID) {
		this.roomID = roomID;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}
	

}
