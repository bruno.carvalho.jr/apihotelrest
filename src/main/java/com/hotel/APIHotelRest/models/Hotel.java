package com.hotel.APIHotelRest.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Hotel {

	public long id;
	
	public String name;
	
	public long cityCode;
	
	public String cityName;
	
	public ArrayList<Room> rooms;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getCityCode() {
		return cityCode;
	}

	public void setCityCode(long cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public ArrayList<Room> getRooms() {
		return rooms;
	}

	public void setRooms(ArrayList<Room> rooms) {
		this.rooms = rooms;
	}

	@SuppressWarnings("deprecation")
	public HotelDetail calcDetailPrice(String checkin, String checkout, Long adult, Long child) throws ParseException {
		
		HotelDetail hotelDetail = new HotelDetail();
		
		int dias = this.calcNumberOfDays(checkin, checkout);
		
		if(dias == 0) {
			
			dias = 1;
		}
		String taxa = "0.70";
		BigDecimal taxaComi = new BigDecimal(taxa);
		taxaComi.setScale(2);
		BigDecimal diasBig = new BigDecimal(dias);
		BigDecimal childBig = new BigDecimal(child);
		BigDecimal adultBig = new BigDecimal(adult);
	
		
		for(Room room : this.rooms) {
			
			RoomDetail roomDetail = new RoomDetail();
			
			roomDetail.priceDetail.pricePerDayAdult = (room.price.adult.multiply(adultBig)).multiply(diasBig);
			
			roomDetail.priceDetail.pricePerDayAdult.setScale(2);
			
			roomDetail.priceDetail.pricePerDayChild = (room.price.child.multiply(childBig)).multiply(diasBig);
			roomDetail.priceDetail.pricePerDayChild.setScale(2);
		
			roomDetail.roomID = room.roomID;
			
			roomDetail.categoryName = room.categoryName;
			
			roomDetail.totalPrice = new BigDecimal(0.00);
			
			roomDetail.totalPrice = (roomDetail.priceDetail.pricePerDayAdult.add(roomDetail.priceDetail.pricePerDayAdult.multiply(taxaComi))).add((roomDetail.priceDetail.pricePerDayChild.add(roomDetail.priceDetail.pricePerDayChild.multiply(taxaComi))));
			
	
			hotelDetail.rooms.add(roomDetail);
		}

		return hotelDetail;
	}

	@SuppressWarnings("deprecation")
	public int calcNumberOfDays(String ckeckin, String checkout) throws ParseException {
		
		Date checkinDate = new SimpleDateFormat("dd/MM/yyyy").parse(ckeckin);
		Date checkouDate = new SimpleDateFormat("dd/MM/yyyy").parse(checkout);
		
		int dias;
		
		for (dias = 0; checkouDate.after(checkinDate); dias++) {  
			checkouDate.setDate(checkouDate.getDate() - 1);  
		}  
		
		return dias;
	}
}